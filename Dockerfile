FROM golang:1.20.1-alpine3.17

RUN mkdir /app

ADD ./countdown.go /app

WORKDIR /app

RUN go env -w GO111MODULE=off

RUN go build -o countdown .

CMD ["/app/countdown"]
