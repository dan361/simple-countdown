package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)


func main() {
	endDate := time.Date(2023, 3, 31, 0, 0, 0, 0, time.UTC)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)


	ticker := time.Tick(time.Second)

	for {
		select {
		case <-stop:
		     fmt.Println("\nProgramm beendet.")
		     return
		case <-ticker:
		     now := time.Now()
		     duration := endDate.Sub(now)

		     if duration <= 0 {
			     fmt.Println("\nCountdown beendet.")
			     return
		     }

		     days := int(duration.Hours() / 24)
		     hours := int(duration.Hours()) % 24
		     minutes := int(duration.Minutes()) % 60
		     seconds := int(duration.Seconds()) % 60

		     fmt.Printf("\rCountdown bis zum 31.03.2023: %d Tage, %d Stunden, %d Minuten und %d Sekunden", days, hours, minutes, seconds)
	     }
     }





}
